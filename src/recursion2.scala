

/**
 * @author Andy
 */
object recursion2 {
	def main(args: Array[String]) {
		val a = 1000000
		val vec = Vector.tabulate(a)(i => i)
		import scala.annotation.tailrec

		def sumVecValues(vector: Vector[Int], vectorLen: Int): Int = {
			@tailrec
			def helper(vector: Vector[Int], vectorLen: Int, sum: Int): Int = {
				if (vectorLen >= 0) {
					helper(vector, vectorLen - 1, sum + vector(vectorLen))
				} else {
					sum
				}
			}
			helper(vector, vectorLen, 0)
		}

		println(sumVecValues(vec, vec.length - 1))
	}
}