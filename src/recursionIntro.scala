/**
 * @author Andy
 */
object recursionIntro {
	def main(args: Array[String]): Unit = {
		import scala.annotation._
		def estimatePi(n: Int): Double = {
			@tailrec
			def helper(n: Int, sum: Int): Double = {
				if (n < 1) sum else {
					val x = math.random
					val y = math.random
					helper(n - 1, sum + (if (x * x + y * y < 1) 1 else 0))
				}
			}
			return helper(n, 0) / n * 4
		}

		println(estimatePi(10000))
	}

}