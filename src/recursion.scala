/**
 * Created by andypham on 8/3/15.
 */
object recursion {
  def main(args: Array[String]) {

    val a = 5

    //def factorial(n:Int): Int = if(n<2) 1 else n*factorial(n-1)

    //println(factorial(a))

    import scala.annotation.tailrec
    def estimatePi (n:Int):Double = {
      @tailrec
      def helper(n:Int, sum: Int):Double = {
        if (n < 1) sum else {
          val x = math.random
          val y = math.random
          helper(n-1, sum+(if(x*x+y*y<1) 1 else 0))
        }
      }
      helper(n,0)/n*4
    }
println(estimatePi(100000))
  }
}
