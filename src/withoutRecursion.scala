

/**
 * @author Andy
 */
object withoutRecursion {
	def main(args: Array[String]): Unit = {
		val a = 1000000
		val vec = Vector.tabulate(a)(i => i)

		def sumVecValues(vector:Vector[Int]):Int = {
    	var i = 0
    	var sum = 0
    	for(i <- vector) {
    		sum = sum + vector(i)
    	}
    	return sum
    }
    
    println(sumVecValues(vec))
	}
}