/**
 * Created by andypham on 8/3/15.
 */
object collections {
  def main(args: Array[String]) {


    val arr = Array(1, 2, 3)
    val lst = List(7, 8, 9)

    //arr(0) = 99

    //println(arr(0))
    //println(lst(0))

    val arr2 = Array.fill(10)(math.random)
    val arr3 = Array.tabulate(10)(i => i*i)

    import scala.annotation.tailrec
    @tailrec
    def listArr(n: Array[Int], len: Int): Unit = {
      if (len >= 0) {
        println(n(len).toString())
        listArr(n, len - 1)
      }
    }

    listArr(arr3, arr3.length-1)

  }

}
