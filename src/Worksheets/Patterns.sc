object Patterns {
  val strs = "Welcome to the Scala worksheet".split(" ")
                                                  //> strs  : Array[String] = Array(Welcome, to, the, Scala, worksheet)
  val lst = List(1,2,3,4,5)                       //> lst  : List[Int] = List(1, 2, 3, 4, 5)
  
  val Array(hour,minute,second) = "01:33:37".split(":")
                                                  //> hour  : String = 01
                                                  //| minute  : String = 33
                                                  //| second  : String = 37
}