object CollectionNotes {
	val arr = Array(1, 2, 3)                  //> arr  : Array[Int] = Array(1, 2, 3)

	val lst = List(4, 5, 6)                   //> lst  : List[Int] = List(4, 5, 6)

	val arr2 = Array.fill(10)(math.random)    //> arr2  : Array[Double] = Array(0.7614760580207349, 0.22161863042270347, 0.525
                                                  //| 1819575292929, 0.01410094183396804, 0.7372885098560352, 0.6497019405051098, 
                                                  //| 0.28717242540245824, 0.9976428071959293, 0.7853227177029334, 0.7196484775962
                                                  //| 083)
	def littleArray(i: => Int): Array[Int] = Array(i, i, i)
                                                  //> littleArray: (i: => Int)Array[Int]

	littleArray(util.Random.nextInt(3))       //> res0: Array[Int] = Array(2, 1, 0)

	val arr3 = Array.tabulate(10)(b => b * b) //> arr3  : Array[Int] = Array(0, 1, 4, 9, 16, 25, 36, 49, 64, 81)

	val vec = Vector.tabulate(10)(b => b * b) //> vec  : scala.collection.immutable.Vector[Int] = Vector(0, 1, 4, 9, 16, 25, 3
                                                  //| 6, 49, 64, 81)

	println(vec(0))                           //> 0
}