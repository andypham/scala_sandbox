object CollectionNotes2 {
	val arr = Array(1, 2, 3)                  //> arr  : Array[Int] = Array(1, 2, 3)

	val lst = List(5.2, 6, 7)                 //> lst  : List[Double] = List(5.2, 6.0, 7.0)

	val arr2 = Array.fill(10)(math.random)    //> arr2  : Array[Double] = Array(0.10192253145628549, 0.7524989087568066, 0.197
                                                  //| 18266105088222, 0.4711118738304165, 0.6114176728489775, 0.9181589976011473, 
                                                  //| 0.14136739666825693, 0.8621204847535974, 0.3480858745722115, 0.3823445287331
                                                  //| 5683)

	val arr3 = Array.tabulate(10)(i => i)     //> arr3  : Array[Int] = Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)

	arr2.take(3)                              //> res0: Array[Double] = Array(0.10192253145628549, 0.7524989087568066, 0.19718
                                                  //| 266105088222)
	arr2.drop(3)                              //> res1: Array[Double] = Array(0.4711118738304165, 0.6114176728489775, 0.918158
                                                  //| 9976011473, 0.14136739666825693, 0.8621204847535974, 0.3480858745722115, 0.3
                                                  //| 8234452873315683)

	val (before, after) = arr3.splitAt(4)     //> before  : Array[Int] = Array(0, 1, 2, 3)
                                                  //| after  : Array[Int] = Array(4, 5, 6, 7, 8, 9)

	arr2.slice(3, 6)                          //> res2: Array[Double] = Array(0.4711118738304165, 0.6114176728489775, 0.918158
                                                  //| 9976011473)

	arr3.product                              //> res3: Int = 0
	arr3.sum                                  //> res4: Int = 45

	lst.toArray                               //> res5: Array[Double] = Array(5.2, 6.0, 7.0)
	lst.toVector                              //> res6: Vector[Double] = Vector(5.2, 6.0, 7.0)
	lst.updated(1, 99)                        //> res7: List[AnyVal] = List(5.2, 99, 7.0)

	arr2.patch(2, Array(1, 2, 3), 3)          //> res8: Array[AnyVal] = Array(0.10192253145628549, 0.7524989087568066, 1, 2, 3
                                                  //| , 0.9181589976011473, 0.14136739666825693, 0.8621204847535974, 0.34808587457
                                                  //| 22115, 0.38234452873315683)
	val arr4 = Array(1,3,5,7,9)               //> arr4  : Array[Int] = Array(1, 3, 5, 7, 9)
	arr4.intersect(arr)                       //> res9: Array[Int] = Array(1, 3)
	
	arr.union(arr4).distinct                  //> res10: Array[Int] = Array(1, 2, 3, 5, 7, 9)
	
	arr4.diff(arr)                            //> res11: Array[Int] = Array(5, 7, 9)
	
	arr4.mkString(", ")                       //> res12: String = 1, 3, 5, 7, 9
}