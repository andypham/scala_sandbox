object HigherOrderMethods {
  val a = Array(1,2,3,4,5)                        //> a  : Array[Int] = Array(1, 2, 3, 4, 5)
  val b = List(5,2,8,1,27,9)                      //> b  : List[Int] = List(5, 2, 8, 1, 27, 9)
  val c = Vector.tabulate(10)(i => i*i)           //> c  : scala.collection.immutable.Vector[Int] = Vector(0, 1, 4, 9, 16, 25, 36,
                                                  //|  49, 64, 81)
  
  a.map(i => i*2)                                 //> res0: Array[Int] = Array(2, 4, 6, 8, 10)
  a.map(_*2)                                      //> res1: Array[Int] = Array(2, 4, 6, 8, 10)
  a.map(_+" is a number")                         //> res2: Array[String] = Array(1 is a number, 2 is a number, 3 is a number, 4 i
                                                  //| s a number, 5 is a number)
  b.filter(_<5)                                   //> res3: List[Int] = List(2, 1)
  
  a.flatMap(i => b.take(i))                       //> res4: Array[Int] = Array(5, 5, 2, 5, 2, 8, 5, 2, 8, 1, 5, 2, 8, 1, 27)
  
  c.exists(_>50)                                  //> res5: Boolean = true
  c.forall(_<100)                                 //> res6: Boolean = true
  
  a.reduceLeft((x,y) => x+y)                      //> res7: Int = 15
  a.reduceLeft(_+_)                               //> res8: Int = 15
  
  a.foldLeft("0")(_+_)                            //> res9: String = 012345
  
  b.find(_%3 == 0).getOrElse(0)                   //> res10: Int = 27
}