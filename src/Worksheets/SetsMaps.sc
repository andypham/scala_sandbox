object SetsMaps {
  import collection.mutable
  val s = Set(1,2,3)                              //> s  : scala.collection.immutable.Set[Int] = Set(1, 2, 3)
  val s2 = s + 4                                  //> s2  : scala.collection.immutable.Set[Int] = Set(1, 2, 3, 4)
  
  val mutSet = mutable.Set(1,2,3)                 //> mutSet  : scala.collection.mutable.Set[Int] = Set(1, 2, 3)
  mutSet +=4                                      //> res0: SetsMaps.mutSet.type = Set(1, 2, 3, 4)
  
  val m = Map("Texas"->"Austin","Ohio"->"Columbus")
                                                  //> m  : scala.collection.immutable.Map[String,String] = Map(Texas -> Austin, Oh
                                                  //| io -> Columbus)
  
  val mutMap = mutable.Map()                      //> mutMap  : scala.collection.mutable.Map[Nothing,Nothing] = Map()
}