object Loops {

	var a = Array.fill(5)(math.random)        //> a  : Array[Double] = Array(0.20147198027767155, 0.2786148886701063, 0.3333212
                                                  //| 866133616, 0.02597710948205123, 0.32909527758949586)
	for (x <- a) yield x * x                  //> res0: Array[Double] = Array(0.040590958837006474, 0.07762625618865573, 0.1111
                                                  //| 0308010958674, 6.74810217042476E-4, 0.10830370173170734)

	a.map(x => x * x)                         //> res1: Array[Double] = Array(0.040590958837006474, 0.07762625618865573, 0.1111
                                                  //| 0308010958674, 6.74810217042476E-4, 0.10830370173170734)
  for(i <- 0 until 5; a = 2*i; j <- 5 until 10) yield(i,j)
                                                  //> res2: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((0,5), (0,6
                                                  //| ), (0,7), (0,8), (0,9), (1,5), (1,6), (1,7), (1,8), (1,9), (2,5), (2,6), (2,
                                                  //| 7), (2,8), (2,9), (3,5), (3,6), (3,7), (3,8), (3,9), (4,5), (4,6), (4,7), (4
                                                  //| ,8), (4,9))
  val partialFunct:PartialFunction[Any,String] = {
  case i:Int => "Number"
  case s:String => "The string "+s
  }                                               //> partialFunct  : PartialFunction[Any,String] = <function1>
  
  partialFunct(1)                                 //> res3: String = Number
  partialFunct("hello world")                     //> res4: String = The string hello world
}